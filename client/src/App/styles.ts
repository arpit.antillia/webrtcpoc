import styled from '@emotion/styled';

export const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  background: black;
  color: white;
`;

export const Video = styled.video`
  width: 70%;
  height: 50%;
  border: 1px solid red;
`;

export const UserId = styled.span`
  font-weight: bold;
  font-size: 1.2rem;
`;

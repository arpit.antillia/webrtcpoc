import { useEffect, useRef, useState, useCallback, FormEvent } from 'react';
import io from 'socket.io-client';

import * as S from './styles';
import PeerIdInput from '../Components/PeerIdInput';

const SERVER_URI = 'http://127.0.0.1:3000';

const CALL_EVENTS = {
  REGISTER: 'REGISTER',
  START_CALL: 'START_CALL',
  ACCEPT_CALL: 'ACCEPT_CALL',
  REJECT_CALL: 'REJECT_CALL',
  END_CALL: 'END_CALL'
};

const MESSAGES = {
  NOT_REGISTERED: 'The user is not registered.',
  REGISTERED: 'REGISTERED',
  CALL_ACCEPTED: 'CALL_ACCEPTED',
  CALL_STARTED: 'CALL_STARTED',
  CALL_REJECTED: 'CALL_REJECTED',
  CALL_ENDED: 'CALL_ENDED'
};

type CallState = 'idle' | 'incoming' | 'ongoing';
// type CallRole = 'initiator' | 'receiver';

const App = () => {
  const [userId, setUserId] = useState('');
  const [peerId, setPeerId] = useState('');
  const [callState, setCallState] = useState<CallState>('idle');
  const [isInitiator, setIsInitiator] = useState(true);

  const localVideo$ = useRef<HTMLVideoElement | null>(null);
  const localStream$ = useRef<MediaStream>();
  const socket$ = useRef<SocketIOClient.Socket>(io.connect(SERVER_URI));

  const pc$ = useRef(
    new RTCPeerConnection({
      iceServers: [{ urls: ['stun:stun.stunprotocol.org'] }]
    })
  );

  const dataChannel$ = useRef<RTCDataChannel | null>(null);

  const createOffer = async (): Promise<void> => {
    const offer = await pc$.current.createOffer();
    await pc$.current.setLocalDescription(offer);
  };

  const createAnswer = async (): Promise<void> => {
    const offer = await pc$.current.createAnswer();
    await pc$.current.setLocalDescription(offer);
  };

  const setRemoteDesc = async (
    sdpAnswer: RTCSessionDescriptionInit
  ): Promise<RTCSessionDescription | null> => {
    console.log('setRemoteDesc');
    await pc$.current.setRemoteDescription(sdpAnswer);

    return pc$.current.remoteDescription;
  };

  // const peerVideo$ = useRef<HTMLVideoElement>(null);
  const getLocalVideo = (): Promise<MediaStream> => {
    const constraints = {
      video: { facingMode: 'user' },
      audio: true
    };

    return navigator.mediaDevices.getUserMedia(constraints);
  };

  const startLocalStream = (stream: MediaStream) => {
    if (!localVideo$.current) {
      return;
    }

    localVideo$.current.srcObject = stream;
    localVideo$.current.muted = true;
    localVideo$.current.play();
  };

  const register = () => {
    socket$.current.emit(CALL_EVENTS.REGISTER);
  };

  const playVideo = useCallback(async () => {
    localStream$.current = await getLocalVideo();
    startLocalStream(localStream$.current);
  }, []);

  const startCall = async () => {
    try {
      console.log('Start Call');
      await createOffer();

      socket$.current.emit(CALL_EVENTS.START_CALL, {
        to: peerId,
        from: userId,
        sdp: pc$.current.localDescription
      });
      setIsInitiator(true);
    } catch (err) {
      console.log(err);
    }
  };

  const acceptCall = async () => {
    try {
      console.log('Accept Call');
      console.log('localDescription', pc$.current.localDescription);
      
      await createAnswer();

      socket$.current.emit(CALL_EVENTS.ACCEPT_CALL, {
        to: peerId,
        from: userId,
        sdp: pc$.current.localDescription
      });
    } catch (err) {
      console.log(err);
    }
  };

  const rejectCall = () => {
    setPeerId('');
    setCallState('idle');
    socket$.current.emit(CALL_EVENTS.REJECT_CALL);
  };

  useEffect(() => {
    pc$.current.onicecandidate = (event) => {
      console.log('NEW ice candidate!! on localConnection reprinting SDP ');

      dataChannel$.current = pc$.current.createDataChannel('sendChannel');
      dataChannel$.current.onmessage = (e) => console.log('message received!!!' + e.data);
      dataChannel$.current.onopen = (e) => console.log('open!!!!');
      dataChannel$.current.onclose = (e) => console.log('closed!!!!');
    };
  }, []);

  useEffect(() => {
    socket$.current.on('connection', () => {
      console.log('connection');
    });

    socket$.current.on('receive', (data: any) => {
      console.log(data);
    });
  }, []);

  useEffect(() => {
    socket$.current.on(MESSAGES.REGISTERED, ({ id }: { id: string }) => {
      console.log(MESSAGES.REGISTERED);

      setUserId(id);
    });
  }, []);

  useEffect(() => {
    type Call = { to: string; from: string; sdp: RTCSessionDescriptionInit };

    socket$.current.on(MESSAGES.CALL_ACCEPTED, async ({ from, to, sdp }: Call) => {
      console.log(MESSAGES.CALL_ACCEPTED);
      console.log({ from, to, sdp });
      if (sdp) {
        await setRemoteDesc(sdp);
        setCallState('ongoing');
      }
    });
  }, []);

  useEffect(() => {
    type Call = { to: string; from: string; sdp: RTCSessionDescriptionInit };
    socket$.current.on(MESSAGES.CALL_STARTED, async ({ to, from, sdp }: Call) => {
      console.log(MESSAGES.CALL_STARTED);
      console.log({ to, from, sdp });

      // TODO: Add identity validation
      if (sdp) {
        await setRemoteDesc(sdp);
        setCallState('incoming');
        setPeerId(from);
      }
    });
  }, []);

  useEffect(() => {
    socket$.current
      .on(MESSAGES.NOT_REGISTERED, () => {
        console.log(MESSAGES.NOT_REGISTERED);
      })
      .on(MESSAGES.CALL_REJECTED, () => {
        console.log(MESSAGES.CALL_REJECTED);
        setIsInitiator(false);
      })
      .on(MESSAGES.CALL_ENDED, () => {
        console.log(MESSAGES.CALL_ENDED);
      });
  }, []);

  useEffect(() => {
    return () => {
      localStream$.current?.getTracks().forEach((track) => track.stop());
    };
  }, []);

  return (
    <S.Wrapper className="App">
      {userId ? (
        <>
          <div>
            Your User Id is - <S.UserId>{userId}</S.UserId>, share with a friend to start call.
          </div>
          <PeerIdInput setPeerId={setPeerId} />
        </>
      ) : (
        <button onClick={register}>Register Account</button>
      )}
      <S.Video ref={localVideo$} />
      {callState === 'incoming' && peerId ? (
        <>
          <div>{peerId} is calling you? </div>
          <button onClick={acceptCall}>Accept</button>
          <button onClick={rejectCall}>Reject</button>
        </>
      ) : null}
      {peerId ? <button onClick={startCall}>Start Call</button> : null}
    </S.Wrapper>
  );
};

export default App;

import { FormEvent, useState } from 'react';

type Props = {
  setPeerId: (val: string) => void;
};
const PeerIdInput: React.FC<Props> = ({ setPeerId }) => {
  const [query, setQuery] = useState('');

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    setPeerId(query);
  };

  return (
    <form onSubmit={onSubmit}>
      <input
        type="text"
        name="peerIdInput"
        placeholder="Enter peerId"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
      />
      <button type="submit">Submit</button>
    </form>
  );
};

export default PeerIdInput;

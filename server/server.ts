import * as express from 'express';
import * as cors from 'cors';

const app = express();

app.use(cors());

import * as http from 'http';
import * as socketIO from 'socket.io';

const server = http.createServer(app);
const io = new socketIO.Server(server, {
  cors: {
    origin: '*'
  }
});

const PORT = 3000;

const CALL_EVENTS = {
  REGISTER: 'REGISTER',
  START_CALL: 'START_CALL',
  ACCEPT_CALL: 'ACCEPT_CALL',
  REJECT_CALL: 'REJECT_CALL',
  END_CALL: 'END_CALL'
};

const MESSAGES = {
  NOT_REGISTERED: 'The user is not registered.',
  REGISTERED: 'REGISTERED',
  CALL_ACCEPTED: 'CALL_ACCEPTED',
  CALL_STARTED: 'CALL_STARTED',
  CALL_REJECTED: 'CALL_REJECTED',
  CALL_ENDED: 'CALL_ENDED'
};

const generateId = () => {
  return Math.random().toString(36).substr(2, 9);
};

const CLIENTS = new Set<string>();

io.on('connection', (socket: socketIO.Socket) => {
  socket.on(CALL_EVENTS.REGISTER, () => {
    console.log(CALL_EVENTS.REGISTER);
    const id = generateId();
    CLIENTS.add(id);

    console.log(CLIENTS);
    socket.emit(MESSAGES.REGISTERED, { id });
  });

  socket.on(
    CALL_EVENTS.START_CALL,
    (call: { to: string; from: string; sdp: string }) => {
      console.log(CALL_EVENTS.START_CALL);
      console.log(call);

      if (!CLIENTS.has(call.from) || !CLIENTS.has(call.to)) {
        socket.emit(MESSAGES.NOT_REGISTERED);
        return;
      }

      socket.broadcast.emit(MESSAGES.CALL_STARTED, {
        to: call.to,
        from: call.from,
        sdp: call.sdp
      });
    }
  );

  socket.on(
    CALL_EVENTS.ACCEPT_CALL,
    (call: { to: string; from: string; sdp: string }) => {
      console.log(CALL_EVENTS.ACCEPT_CALL);

      if (!CLIENTS.has(call.from) || !CLIENTS.has(call.to)) {
        socket.emit(MESSAGES.NOT_REGISTERED);
        return;
      }
      console.log(call);

      socket.broadcast.emit(MESSAGES.CALL_ACCEPTED, {
        to: call.from,
        from: call.to,
        sdp: call.sdp
      });
    }
  );

  socket.on(CALL_EVENTS.REJECT_CALL, (call) => {
    console.log(CALL_EVENTS.REJECT_CALL);

    socket.emit(MESSAGES.CALL_REJECTED, { rejected_by: call.to });
  });

  socket.on(CALL_EVENTS.END_CALL, (call) => {
    console.log(CALL_EVENTS.END_CALL);

    socket.emit(MESSAGES.CALL_REJECTED, { ended_by: call.to });
  });
});

server.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});
